import { ReactNode } from 'react';
import GoTrue, {
  User as GoTrueUser,
  Settings as GoTrueSettings,
} from 'gotrue-js';
import { TokenParam } from './token';
declare type authChangeParam = (user?: User) => string | void;
export declare type Settings = GoTrueSettings;
export declare type User = GoTrueUser;
declare type Provider = 'bitbucket' | 'github' | 'gitlab' | 'google';
export declare type ReactNetlifyIdentityAPI = {
  user: User | undefined;
  /** not meant for normal use! you should mostly use one of the other exported methods to update the user instance */
  setUser: (_user: GoTrueUser | undefined) => GoTrueUser | undefined;
  isConfirmedUser: boolean;
  isLoggedIn: boolean;
  signupUser: (
    email: string,
    password: string,
    data: Object,
    directLogin?: boolean
  ) => Promise<User | undefined>;
  loginUser: (
    email: string,
    password: string,
    remember?: boolean
  ) => Promise<User | undefined>;
  logoutUser: () => Promise<User | undefined>;
  requestPasswordRecovery: (email: string) => Promise<void>;
  recoverAccount: (remember?: boolean) => Promise<User | undefined>;
  updateUser: (fields: object) => Promise<User | undefined>;
  getFreshJWT: () => Promise<string> | undefined;
  authedFetch: {
    get: (endpoint: string, obj?: RequestInit) => Promise<any>;
    post: (endpoint: string, obj?: RequestInit) => Promise<any>;
    put: (endpoint: string, obj?: RequestInit) => Promise<any>;
    delete: (endpoint: string, obj?: RequestInit) => Promise<any>;
  };
  _goTrueInstance: GoTrue;
  _url: string;
  loginProvider: (provider: Provider) => void;
  acceptInviteExternalUrl: (
    provider: Provider,
    autoRedirect: boolean
  ) => string | undefined;
  settings: Settings;
  param: TokenParam;
  verifyToken: () => Promise<User | undefined>;
};
export declare const useIdentityContext: () => ReactNetlifyIdentityAPI;
/** most people should use this provider directly */
export declare function IdentityContextProvider({
  url,
  children,
  onAuthChange,
}: {
  url: string;
  children: ReactNode;
  onAuthChange?: authChangeParam;
}): JSX.Element;
/** some people may want to use this as a hook and bring their own contexts */
export declare function useNetlifyIdentity(
  url: string,
  onAuthChange?: authChangeParam,
  enableRunRoutes?: boolean
): ReactNetlifyIdentityAPI;
export {};

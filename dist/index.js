'use strict';

if (process.env.NODE_ENV === 'production') {
  module.exports = require('./react-netlify-identity.cjs.production.min.js');
} else {
  module.exports = require('./react-netlify-identity.cjs.development.js');
}

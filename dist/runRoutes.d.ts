import GoTrue, { User } from 'gotrue-js';
import { TokenParam } from './token';
export declare function runRoutes(
  gotrue: GoTrue,
  setUser: (value: User) => User | undefined,
  remember?: boolean
): TokenParam;

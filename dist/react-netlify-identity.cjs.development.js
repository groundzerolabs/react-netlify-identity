'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault(ex) {
  return ex && typeof ex === 'object' && 'default' in ex ? ex['default'] : ex;
}

var React = require('react');
var React__default = _interopDefault(React);
var GoTrue = _interopDefault(require('gotrue-js'));

function _extends() {
  _extends =
    Object.assign ||
    function(target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];

        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }

      return target;
    };

  return _extends.apply(this, arguments);
}

var defaultParam = {
  token: undefined,
  type: undefined,
  error: undefined,
  status: undefined,
};

/**
 * This code runs on every rerender so keep it light
 * keep checking the current route and do logic based on the route
 * as dictated by netlify identity's communication with us via hashes
 */

var routes = /(confirmation|invite|recovery|email_change|access)_token=([^&]+)/;
var errorRoute = /error=access_denied&error_description=403/;

var reduceHashToKeyValue = function reduceHashToKeyValue(hash) {
  return hash.split('&').reduce(function(carry, pair) {
    var _extends2;

    var _pair$split = pair.split('='),
      key = _pair$split[0],
      value = _pair$split[1];

    return _extends(
      {},
      carry,
      ((_extends2 = {}), (_extends2[key] = value), _extends2)
    );
  }, {});
};

var hashReplace = /^#\/?/;
function runRoutes(gotrue, setUser, remember) {
  var _document, _document$location;

  if (remember === void 0) {
    remember = true;
  }

  // early terminate if no hash
  // also accounts for document.cookie further down
  if (
    !((_document = document) === null || _document === void 0
      ? void 0
      : (_document$location = _document.location) === null ||
        _document$location === void 0
      ? void 0
      : _document$location.hash)
  ) {
    return defaultParam;
  }

  var hash = document.location.hash.replace(hashReplace, '');

  try {
    history.pushState(
      '',
      document.title,
      window.location.pathname + window.location.search
    );
  } catch (_) {
    window.location.href.substr(0, window.location.href.indexOf('#'));
  } // earliest possible bail on any match

  if (hash.match(errorRoute)) {
    return _extends({}, defaultParam, {
      error: 'access_denied',
      status: 403,
    });
  }

  var matchesActionHashes = hash.match(routes);

  if (matchesActionHashes) {
    var params = reduceHashToKeyValue(hash);

    if (params.confirmation_token) {
      gotrue
        .confirm(params.confirmation_token, remember)
        .then(setUser)
        ['catch'](console.error); // dont notify dev as this package does not export its own method for this

      return defaultParam;
    }

    if (params.access_token) {
      document.cookie = 'nf_jwt=' + params.access_token;
      gotrue
        .createUser(params, remember)
        .then(setUser)
        ['catch'](console.error); // also dont notify dev here for the same reasons as above

      return defaultParam;
    } // pass responsibility to dev in all other cases

    return _extends({}, defaultParam, {
      type: matchesActionHashes[1],
      token: matchesActionHashes[2],
    });
  }

  return defaultParam;
}

var defaultSettings = {
  autoconfirm: false,
  disable_signup: false,
  external: {
    bitbucket: false,
    email: true,
    facebook: false,
    github: false,
    gitlab: false,
    google: false,
  },
};
var errors = {
  noUserFound: 'No current user found - are you logged in?',
  noUserTokenFound: 'no user token found',
  tokenMissingOrInvalid: 'either no token found or invalid for this purpose',
};

var _createCtx = /*#__PURE__*/ createCtx(),
  _useIdentityContext = _createCtx[0],
  _IdentityCtxProvider = _createCtx[1];

var useIdentityContext = _useIdentityContext; // we dont want to expose _IdentityCtxProvider

/** most people should use this provider directly */

function IdentityContextProvider(_ref) {
  var url = _ref.url,
    children = _ref.children,
    _ref$onAuthChange = _ref.onAuthChange,
    onAuthChange =
      _ref$onAuthChange === void 0 ? function() {} : _ref$onAuthChange;

  /******** SETUP */
  if (!url || !validateUrl(url)) {
    // just a safety check in case a JS user tries to skip this
    throw new Error(
      'invalid netlify instance URL: ' +
        url +
        '. Please check the docs for proper usage or file an issue.'
    );
  }

  var identity = useNetlifyIdentity(url, onAuthChange);
  return React__default.createElement(
    _IdentityCtxProvider,
    {
      value: identity,
    },
    children
  );
}
/** some people may want to use this as a hook and bring their own contexts */

function useNetlifyIdentity(url, onAuthChange, enableRunRoutes) {
  if (onAuthChange === void 0) {
    onAuthChange = function onAuthChange() {};
  }

  if (enableRunRoutes === void 0) {
    enableRunRoutes = true;
  }

  var goTrueInstance = React.useMemo(
    function() {
      return new GoTrue({
        APIUrl: '' + url,
        setCookie: true,
      });
    },
    [url]
  );
  /******* STATE and EFFECTS */

  var _useState = React.useState(goTrueInstance.currentUser() || undefined),
    user = _useState[0],
    setUser = _useState[1];

  var _setUser = React.useCallback(
    function(_user) {
      setUser(_user);
      onAuthChange(_user); // if someone's subscribed to auth changes, let 'em know

      return _user; // so that we can continue chaining
    },
    [onAuthChange]
  );

  var _useState2 = React.useState(defaultParam),
    param = _useState2[0],
    setParam = _useState2[1];

  React.useEffect(function() {
    if (enableRunRoutes) {
      var _param = runRoutes(goTrueInstance, _setUser);

      if (_param.token || _param.error) {
        setParam(_param);
      }
    }
  }, []);

  var _useState3 = React.useState(defaultSettings),
    settings = _useState3[0],
    setSettings = _useState3[1];

  React.useEffect(function() {
    goTrueInstance.settings
      .bind(goTrueInstance)()
      .then(function(x) {
        return setSettings(x);
      });
  }, []);
  /******* OPERATIONS */
  // make sure the Registration preferences under Identity settings in your Netlify dashboard are set to Open.
  // https://react-netlify-identity.netlify.com/login#access_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTY0ODY3MjEsInN1YiI6ImNiZjY5MTZlLTNlZGYtNGFkNS1iOTYzLTQ4ZTY2NDcyMDkxNyIsImVtYWlsIjoic2hhd250aGUxQGdtYWlsLmNvbSIsImFwcF9tZXRhZGF0YSI6eyJwcm92aWRlciI6ImdpdGh1YiJ9LCJ1c2VyX21ldGFkYXRhIjp7ImF2YXRhcl91cmwiOiJodHRwczovL2F2YXRhcnMxLmdpdGh1YnVzZXJjb250ZW50LmNvbS91LzY3NjQ5NTc_dj00IiwiZnVsbF9uYW1lIjoic3d5eCJ9fQ.E8RrnuCcqq-mLi1_Q5WHJ-9THIdQ3ha1mePBKGhudM0&expires_in=3600&refresh_token=OyA_EdRc7WOIVhY7RiRw5w&token_type=bearer

  /******* external oauth */

  /**
   * @see https://github.com/netlify/gotrue-js/blob/master/src/index.js#L71
   */

  var loginProvider = React.useCallback(
    function(provider) {
      var url = goTrueInstance.loginExternalUrl(provider);
      window.location.href = url;
    },
    [goTrueInstance]
  );
  /**
   * @see https://github.com/netlify/gotrue-js/blob/master/src/index.js#L92
   */

  var acceptInviteExternalUrl = React.useCallback(
    function(provider, autoRedirect) {
      if (autoRedirect === void 0) {
        autoRedirect = true;
      }

      if (!param.token || param.type !== 'invite') {
        console.error(errors.tokenMissingOrInvalid);
        return;
      }

      var url = goTrueInstance.acceptInviteExternalUrl(provider, param.token); // clean up consumed token

      setParam(defaultParam);

      if (autoRedirect) {
        window.location.href = url;
        return;
      }

      return url;
    },
    [goTrueInstance, param]
  );
  /**
   * @see https://github.com/netlify/gotrue-js/blob/master/src/index.js#L123
   */

  var verifyToken = React.useCallback(
    function() {
      if (!param.type || !param.token) {
        return Promise.reject(errors.tokenMissingOrInvalid);
      }

      return goTrueInstance
        .verify(param.type, param.token)
        .then(function(user) {
          // cleanup consumed token
          setParam(defaultParam);
          return user;
        });
    },
    [goTrueInstance, param]
  );
  /******* email auth */

  /**
   * @see https://github.com/netlify/gotrue-js/blob/master/src/index.js#L50
   */

  var signupUser = React.useCallback(
    function(email, password, data, directLogin) {
      if (directLogin === void 0) {
        directLogin = true;
      }

      return goTrueInstance.signup(email, password, data).then(function(user) {
        if (directLogin) {
          return _setUser(user);
        }

        return user;
      });
    },
    [goTrueInstance, _setUser]
  );
  /**
   * @see https://github.com/netlify/gotrue-js/blob/master/src/index.js#L57
   */

  var loginUser = React.useCallback(
    function(email, password, remember) {
      if (remember === void 0) {
        remember = true;
      }

      return goTrueInstance.login(email, password, remember).then(_setUser);
    },
    [goTrueInstance, _setUser]
  );
  /**
   * @see https://github.com/netlify/gotrue-js/blob/master/src/index.js#L80
   */

  var requestPasswordRecovery = React.useCallback(
    function(email) {
      return goTrueInstance.requestPasswordRecovery(email);
    },
    [goTrueInstance]
  );
  /**
   * @see https://github.com/netlify/gotrue-js/blob/master/src/index.js#L87
   */

  var recoverAccount = React.useCallback(
    function(remember) {
      if (!param.token || param.type !== 'recovery') {
        return Promise.reject(errors.tokenMissingOrInvalid);
      }

      return goTrueInstance
        .recover(param.token, remember)
        .then(function(user) {
          return _setUser(user);
        })
        ['finally'](function() {
          // clean up consumed token
          setParam(defaultParam);
        });
    },
    [goTrueInstance, _setUser, param]
  );
  /**
   * @see https://github.com/netlify/gotrue-js/blob/master/src/user.js#L54
   */

  var updateUser = React.useCallback(
    function(fields) {
      if (!user) {
        return Promise.reject(errors.noUserFound);
      }

      return user
        .update(fields) // e.g. { email: "example@example.com", password: "password" }
        .then(_setUser);
    },
    [user]
  );
  /**
   * @see https://github.com/netlify/gotrue-js/blob/master/src/user.js#L63
   */

  var getFreshJWT = React.useCallback(
    function() {
      if (!user) {
        return Promise.reject(errors.noUserFound);
      }

      return user.jwt();
    },
    [user]
  );
  /**
   * @see https://github.com/netlify/gotrue-js/blob/master/src/user.js#L71
   */

  var logoutUser = React.useCallback(
    function() {
      if (!user) {
        return Promise.reject(errors.noUserFound);
      }

      return user.logout().then(function() {
        return _setUser(undefined);
      });
    },
    [user]
  );

  var genericAuthedFetch = function genericAuthedFetch(method) {
    return function(endpoint, options) {
      var _user$token;

      if (options === void 0) {
        options = {};
      }

      if (
        !(user === null || user === void 0
          ? void 0
          : (_user$token = user.token) === null || _user$token === void 0
          ? void 0
          : _user$token.access_token)
      ) {
        return Promise.reject(errors.noUserTokenFound);
      }

      var defaultObj = {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + user.token.access_token,
        },
      };
      var finalObj = Object.assign(
        defaultObj,
        {
          method: method,
        },
        options
      );
      return fetch(endpoint, finalObj).then(function(res) {
        return finalObj.headers['Content-Type'] === 'application/json'
          ? res.json()
          : res;
      });
    };
  };

  var authedFetch = {
    get: genericAuthedFetch('GET'),
    post: genericAuthedFetch('POST'),
    put: genericAuthedFetch('PUT'),
    delete: genericAuthedFetch('DELETE'),
  };
  /******* hook API */

  return {
    user: user,

    /** not meant for normal use! you should mostly use one of the other exported methods to update the user instance */
    setUser: _setUser,
    isConfirmedUser: !!(user && user.confirmed_at),
    isLoggedIn: !!user,
    signupUser: signupUser,
    loginUser: loginUser,
    logoutUser: logoutUser,
    requestPasswordRecovery: requestPasswordRecovery,
    recoverAccount: recoverAccount,
    updateUser: updateUser,
    getFreshJWT: getFreshJWT,
    authedFetch: authedFetch,
    _goTrueInstance: goTrueInstance,
    _url: url,
    loginProvider: loginProvider,
    acceptInviteExternalUrl: acceptInviteExternalUrl,
    settings: settings,
    param: param,
    verifyToken: verifyToken,
  };
}
/**
 *
 *
 * Utils
 *
 */

function validateUrl(value) {
  return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(
    value
  );
} // lazy initialize contexts without providing a Nullable type upfront

function createCtx() {
  var ctx = React.createContext(undefined);

  function useCtx() {
    var c = React.useContext(ctx);
    if (!c) throw new Error('useCtx must be inside a Provider with a value');
    return c;
  }

  return [useCtx, ctx.Provider];
} // // Deprecated for now
// interface NIProps {
//   children: any
//   url: string
//   onAuthChange?: authChangeParam
// }
// export default function NetlifyIdentity({ children, url, onAuthChange }: NIProps) {
//   return children(useNetlifyIdentity(url, onAuthChange))
// }

exports.IdentityContextProvider = IdentityContextProvider;
exports.useIdentityContext = useIdentityContext;
exports.useNetlifyIdentity = useNetlifyIdentity;
//# sourceMappingURL=react-netlify-identity.cjs.development.js.map
